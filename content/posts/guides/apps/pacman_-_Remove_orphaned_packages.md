---
title: pacman - Remove orphaned packages
date: 2021-06-19T20:05:24+01:00
categories: ["guides"]
tags: ["pacman"]
---
Query ( Q ) the database for neither required nor optionally required ( t ) packages,  nor dependencies for builds ( d ), and output less information ( q ). Pass this list ( | ) to be as an argument ( - ). This list is to be searched ( s ) for locally installed packages, and remove ( R ) these packages which are native ( n ) to the system.

```
sudo pacman -Qtdq | sudo pacman -Rns -
```
