---
title: Vim - Commands
date: 2021-06-18T00:30:59+01:00
categories: ["guides"]
tags: ["Vim"]
---
## Notation

Notation | Meaning
---|---
`Key 1` + `Key 2` | Hold `Key 1` and press `Key 2`
`Key 1` , `Key 2` | Press `Key 1`, then press `Key 2`

Modes are shown by their keybindings.

Binding | Meaning
---|---
\- | Applies to all modes (Do not enter `-`)
`Esc` | Normal mode
`Esc` , `:` | Command mode
`Esc` , `i` | Insert mode
`Esc` , `v` | Visual mode 

# Shortcuts

Operation | Mode | Command 
---|---|---
Move to between tabs | - | `Ctrl` + `Pg Up` or `Ctrl` + `Pg Dn`
Copy a line | `Esc` | `yy`
Cut a line | `Esc` | `dd`
Paste at the cursor | `Esc` | `P`
Paste after the cursor | `Esc` | `p`
Go to file (the path is under the cursor) | `Esc` | `gf`
Go to the previous cursor location (may be in a different file) | `Esc` | `Ctrl` + `o`
Go to the next cursor location (may be in a different file) | `Esc` | `Ctrl` + `i`
Replace a character | `Esc` | `r`
Open a file in a new tab |`Esc` , `:` | `tabnew /path/to/file`
Move to the end of a line | `Esc` , `i` | `Ctrl` + `o` , `$`
Select word | `Esc`, `v` | `iw`
Turn off highlighting for the last searched word | `Esc` , `:` | `noh`
Replace the start the selected line(s) with `# ` (Useful for commenting lines out in bash) | `Esc` , `v` | `s/^/# /`  
Paste into the buffer | `Esc` , `:` | `Ctrl` + `r` , `+`
Undo | `Esc` | `u`
Redo | `Esc` | `Ctrl` + `r`
Count words | `Esc` , `v` | `g` , `Ctrl` + `g`
Enclose line in parentheses/brackets (`()`) | `Esc` | `bcw()` , `Esc` , `P`
Record a macro (`...`) , binded to `a` | `Esc` | `qa`, `...` , `Esc`, `q`

## Additional shortcuts

Operation | Mode| Command | Requirement
---|---|---|---
List spelling options for a word | `Esc` | `z=` | `set spell=on` in *init.vim* file
Show live preview of markdown file | `Esc` , `:` | `MarkdownPreview` | Install the plugin: 'iamcco/markdown-preview.nvim'
Compile code | `Esc` , `:` | `QuickRun` | Install the plugin: 'thinca/vim-quickrun'
