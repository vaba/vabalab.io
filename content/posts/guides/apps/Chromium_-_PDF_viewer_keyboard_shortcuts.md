---
title: Chromium - PDF viewer keyboard shortcuts
date: 2021-06-21T00:13:40+01:00
categories: ["guides"]
tags: ["Chromium"]
---
- Zoom in: `CTRL`+`+`
- Zoom out: `CTRL`+`-`
- Toggle between Fit Width and Fit Page: `CTRL`+`\`
- Rotate clockwise: `CTRL`+`]`
- Rotate counter-clockwise: `CTRL`+`[`
- Download PDF: `CTRL`+`S`
- Print PDF: `CTRL`+`P`
- Scroll one page up: `PgUp`
- Scroll one page down: `PgDn`
- Search Document: `CTRL`+`F`
