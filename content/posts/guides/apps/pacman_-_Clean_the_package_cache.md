---
title: pacman - Clean the package cache
date: 2021-06-19T20:05:38+01:00
categories: ["guides"]
tags: ["pacman"]
---
remove ( r ) packages from the cache, but keep ( k ) one ( 1 ) cached version of each package. 
```
paccache -rk 1
```
