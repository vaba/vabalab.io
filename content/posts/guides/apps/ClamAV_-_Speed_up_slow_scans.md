---
title: ClamAV - Speed up slow scans
date: 2021-06-17T19:14:03+01:00
categories: ["guides"]
tags: ["ClamAV"]
---
Your ClamAV scans may be slow because they are being performed as single-thread operations. Try using multi-threading to speed up your scans, which can be achieved with or without the daemon running.

To perform a multi-thread scan on the file with the path */path/to/file*, try the following.

If the daemon is active, use the following command:

```
$ clamdscan --multiscan --fdpass "/path/to/file"
```

If the daemon either is or is not active, use the following command:

```
$ find "/path/to/file" -type f -print0 | xargs -0 -P $(nproc) clamscan
```
