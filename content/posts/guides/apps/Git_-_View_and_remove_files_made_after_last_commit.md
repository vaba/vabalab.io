---
title: Git - View and remove files made after last commit
date: 2021-06-17T18:57:12+01:00
categories: ["guides"]
tags: ["Git"]
summary: 
---

To delete newly made files in the directory *"dir"*:

Enter the directory:
```
cd dir
```
List the newly created files in the directory:
```
git clean -nd
```
Delete the listed files:
``` 
git clean -fd
```
