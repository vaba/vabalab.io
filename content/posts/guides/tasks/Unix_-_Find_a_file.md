---
title: Unix - Find a file
date: 2021-04-22T13:46:55+01:00
categories: ["guides"]
tags: ["Unix"]
---
To search for a file named *filename* in the directory *path/to/search/* use the command:

```
$ find /path/to/search -name "filename" 
```
So to search for the file *hello.md* in the directory *~/Desktop/* we would use:

```
$ find ~/Desktop/ -name "hello.md"
```

We can also search for file patterns. For example, if we knew the file was named *hello*, but didn't know the extension type, we could use the command

```
$ find ~/Desktop/ -name "hello.*"
```

Alternatively, if we were unsure which directory the file with the name *hello* was in, but we knew it had to be inside the home directory *~/*, we could use the command:

```
$ find ~/ -name "**/hello.*"
```
