---
title: About
type: about
hidden: true
layout: about
---

#### Contact info

Email address: [website.chatteration@8shield.net](mailto:website.chatteration@8shield.net)

If you have any feedback about this website, such as errors or suggestions, or just want to get in touch with me, send me an email at the address above.

#### Projects

##### Website

See the [source code](https://gitlab.com/vaba/vaba.gitlab.io) for this website.


##### notepac

I am also the developer of the software [notepac](https://gitlab.com/vaba/notepac), a command line tool which allows users to easily make notes about applications on their system.
