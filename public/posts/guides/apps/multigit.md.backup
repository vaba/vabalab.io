# Pre

## Notation

$ - terminal prompt

[ ] - citation

## Examples

**Git accounts**

**Git repository manager**

In this document we use [GitLab](https://gitlab.com). Alternatives include GitLab and BitBucket.

**SSH key type**

*RSA* is used in this document. Check with your *Git repository manager* for the types of key they accept/ recommend (E.g the recommended SSH key typ	es for GitLab are discussed here: [https://docs.gitlab.com/ee/ssh/](https://docs.gitlab.com/ee/ssh/)). Alternatives key types include *ED25519* and *EDCSA*.

# Generate SSH key

**If** you already have a git account on your machine, see if you have any generated SSH keys for it. To do so, look in the *~/ssh* directory for files with the pattern *id_rsa* using: 

```
$ ls -al ~/.ssh
```
---

**If** the files exist, and are named *id_rsa*, rename them to reflect the git account they are for. **e.g** for a *work* account, do: 

```
$ mv ~/.ssh/id_rsa ~/.ssh/id_rsa-work
```
---

Now get the email for the git account you want to add. **e.g** for a GitLab account, Go to GitLab and sign in your account -> account settings; and copy the gitlab email address.

Generate the SSH key for your git email (**replace *git-email*** with your email), to a file which you will recognise. E.g for a personal account, you could use the filename id_rsa-personal :

```
$ ssh-keygen -t rsa -C git-email -f id_rsa-personal
```

---

Following the examples above, your  *~/.ssh* should include your distinctly named git SSH key files. E.g for the accounts mentioned above:

```
$ ls ~/.ssh
id_rsa-work  id_rsa-work.pub  id_rsa-personal id_rsa-personal.pub
```

## Add SSH key to git repository manager

Copy the newly added public SSH key to your clipboard. You can get the string using `cat`. E.g to get the public SSH key for the *personal* git account, use:

```
$ cat ~/.ssh/id_rsa-personal.pub
```


# Associate the SSH keys with your accounts in git [1]

Open the file *~/.ssh/config* (create it first if it does not exist) with a text editor and insert the following:

```
# work account identity
Host work.gitlab.com
    Hostname gitlab.com
    PreferredAuthentications publickey
    IdentityFile ~/.ssh/id_rsa-work

# personal account identity
Host personal.gitlab.com
    Hostname gitlab.com
    PreferredAuthentications publickey
    IdentityFile ~/.ssh/id_rsa-personal
```

# Managing repositories

For the following examples, we use a repository with ssh address *git@gitlab.com:ecorp/takeover.git* (where *ecorp* is the gitlab username, and *takeover* is the repository name).

## Clone
To clone a repository and have it associated with a specific account, only one small adaptation is required.
Say you want to clone the example repository and associate it with your *work* account. 
Instead of the usual clone command `git clone git@gitlab.com:ecorp/takeover.git`, point to your work account using `work.gitlab.com` in the command like so:

```
$ git clone git@work.gitlab.com:ecorp/takeover.git
```

## Update an existing repository

A similarly command is required to associate an existing repository with an account. To update an existing repository (already cloned to your system) which has the remote name *origin* (to find the remote names associated with the repository, run `git remote` in the repository directory), use the following command:

```
$ git remote set-url origin git@work.gitlab.com:ecorp/takeover.git
```


# Problems

## error when using `git push` [2]


If you get the following error:

```
client_global_hostkeys_private_confirm: server gave bad signature for RSA key 0
```

for example, when you attempt to push your local changes to the remote repository (using `git push`), add the following line to each of your account configuration blocks in *~/.ssh/config*:

```
UpdateHostKeys no 
```

So for the *config* file above, it would now appear as:

```
# work account identity
Host work.gitlab.com
    Hostname gitlab.com
    PreferredAuthentications publickey
    IdentityFile ~/.ssh/id_rsa-work
    UpdateHostKeys no

# personal account identity
Host personal.gitlab.com
    Hostname gitlab.com
    PreferredAuthentications publickey
    IdentityFile ~/.ssh/id_rsa-personal
    UpdateHostKeys no
```

Sources:

[1] https://docs.gitlab.com/ee/ssh/

[2] https://www.reddit.com/r/archlinux/comments/lyazre/openssh_update_causes_problems/gpuwe3x?utm_source=share&utm_medium=web2x&context=3