Restore your system using backed-up files.

# Set-up

Enter the backup folder which contains all the files necessary to restore the system.

```
cd $backupfolder
```

# Install packages

All of the packages installed are taken from a list with their name and version number. We cut out the version number as rolling distros update so frequently that there may be dependency errors if we do not use the most recently available version of a package

## Native

Install all native packages via `pacman`.

```
sudo sh -c 'cat natpkg.txt | cut "-d " -f1 |  xargs pacman -S --needed --noconfirm'
```

## AUR

### Install AUR helper

AUR helper of choice is `paru`. The binary version is being installed to save a lot of time in compiling.

```
sudo pacman -S --needed base-devel # Install dependency (for git or makepkg?) if not installed
git clone https://aur.archlinux.org/paru-bin.git # Clone the package from git
cd ~/paru-bin
makepkg -si # Make the 'paru-bin' package
rm -rf ~/paru-bin # Remove the git folder
```

### Install AUR packages

Install packages via the AUR helper.

```
sh -c 'cat forpkg.txt | cut "-d " -f1 | xargs paru -Tu | xargs paru -S --needed --noconfirm' 
```

## TeX

Download the TeX packages via `tllocalmgr`. We cannot install these packages because we cannot pass a flag to auto confirm the installation of each package.

```
sh -c 'cat forpkg.txt | cut "-d " -f1 | grep texlive-local- | sed 's/texlive-local-//g' | xargs tllocalmgr install' # 
```

Install the TeX packages using `pacman` from their location on the system.

```
sh -c 'cat forpkg.txt | cut "-d " -f1 | grep texlive-local- | sed 's/texlive-local-//g' | find ~/.texlive/texmf-var/arch/builds/ -name *.zst | xargs sudo pacman -U --needed --noconfirm' # Find and install the downloaded packages via pacman
```

## Import config files

Copy the files in the backup directory to the system's home folder using `rsync`.


```
rsync -av --progress ./ ~/ --exclude=forpkg.txt --exclude=natpkg.txt --exclude=.stignore --exclude=.stfolder
```

# Configuration

## jupyter

Install `jupyterlab` via pip.

```
pip install jupyterlab
```

TODO

- Install extensions

## shell

### Make fish default shell

Find the location of the `fish` executable on your system (should be */usr/bin/fish* or */usr/local/bin/fish*).

```
which fish
```

See if `fish` has already been added to the list of available shells in */etc/shells*.

```
cat /etc/shells
```

If the `fish` executable path is in the file, then skip this next step. If it is not, add the path to the list of available shells.

```
echo $(which fish) | sudo tee -a /etc/shells
```

Finally, change the shell to `fish`.

```
chsh -s $(which fish)
```